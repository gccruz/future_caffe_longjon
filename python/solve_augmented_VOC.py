from __future__ import division
import os
import sys
import caffe
import numpy as np
import subprocess

# make a bilinear interpolation kernel
# credit @longjon
def upsample_filt(size):
    factor = (size + 1) // 2
    if size % 2 == 1:
        center = factor - 1
    else:
        center = factor - 0.5
    og = np.ogrid[:size, :size]
    return (1 - abs(og[0] - center) / factor) * \
           (1 - abs(og[1] - center) / factor)

# set parameters s.t. deconvolutional layers compute bilinear interpolation
# N.B. this is for deconvolution without groups
def interp_surgery(net, layers):
    for l in layers:
        m, k, h, w = net.params[l][0].data.shape
        if m != k:
	    print l
	    print 'm and k: ', m , k
            print 'input + output channels need to be the same'
            raise
        if h != w:
            print 'filters need to be square'
            raise
        filt = upsample_filt(h)
        net.params[l][0].data[range(m), range(k), :, :] = filt

# base net -- the learned coarser model
#base_weights = 'snapshots/train_ciafa_batch1_lr1e7_halfres_iter_1000.caffemodel'
base_weights = 'fcn-alexnet-pascal.caffemodel'

# init
caffe.set_mode_gpu()
caffe.set_device(0)

solver = caffe.SGDSolver('solver/augmented_VOC_solver.prototxt')


# do net surgery to set the deconvolution weights for bilinear interpolation
interp_layers = [k for k in solver.net.params.keys() if 'up' in k]
interp_surgery(solver.net, interp_layers)

# copy base weights for fine-tuning
solver.net.copy_from(base_weights)

# solve straight through -- a better approach is to define a solving loop to
# 1. take SGD steps
# 2. score the model by the test net `solver.test_nets[0]`
# 3. repeat until satisfied
#solver.step(50000)


## control layer's initialization
halt_training = False
for layer in solver.net.params.keys():
  print(layer)
  for index in range(0, 2):
    if len(solver.net.params[layer]) < index+1:
      continue

    if np.sum(solver.net.params[layer][index].data) == 0:
      print layer + ' is composed of zeros!'
      halt_training = True

if halt_training:
  print 'Exiting.'
  exit()

#test_interval = 5

solver.step(50000)


#FNULL = open(os.devnull, 'w')
#for i in xrange(100000):
#    solver.step(1)

#    if i > 0 and (i % test_interval) == 0:
#      subprocess.call(['python', 'test_model.py', str(i)], stderr=FNULL)

