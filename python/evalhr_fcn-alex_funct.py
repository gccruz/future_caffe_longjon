import numpy as np
from PIL import Image

import caffe
import matplotlib.pyplot as plt
import time

def vis_square(data):
    """Take an array of shape (n, height, width) or (n, height, width, 3)
       and visualize each (height, width) thing in a grid of size approx. sqrt(n) by sqrt(n)"""
    
    # normalize data for display
    data = (data - data.min()) / (data.max() - data.min())

    # force the number of filters to be square
    n = int(np.ceil(np.sqrt(data.shape[0])))
    print "n: ", n
    padding = (((0, n ** 2 - data.shape[0]),
               (0, 1), (0, 1))                 # add some space between filters
               + ((0, 0),) * (data.ndim - 3))  # don't pad the last dimension (if there is one)
    data = np.pad(data, padding, mode='constant', constant_values=1)  # pad with ones (white)
    
    # tile the filters into an image
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])

    plt.imshow(data); plt.axis('off')
    plt.show()


# load image, switch to BGR, subtract mean, and make dims C x H x W for Caffe
im = Image.open('input_hr.jpg')
in_ = np.array(im, dtype=np.float32)
in_ = in_[:,:,::-1]
in_ -= np.array((104.00698793,116.66876762,122.67891434))
in_ = in_.transpose((2,0,1))


caffe.set_mode_gpu()
caffe.set_device(0)
# load net
#net = caffe.Net('fcn-alex_deploy_var_copy.prototxt', 'train_iter_19000.caffemodel', caffe.TEST)
#net = caffe.Net('fcn-alex_deploy_','train_ciafabatch1lr1e9_3rd_part_iter_40000.caffemodel', caffe.TEST)
net = caffe.Net('fcn-alex_deploy_var_hr_boat.prototxt','train_ciafa_batch1_lr1e5_iter_200.caffemodel', caffe.TEST)

# shape for input (data blob is N x C x H x W), set data
net.blobs['data'].reshape(1, *in_.shape)
net.blobs['data'].data[...] = in_
# run net and take argmax for prediction
net.forward()
start_time=time.time()
net.forward()
#print "elapsed time :" time.time()-start_time

out = net.blobs['score'].data[0].argmax(axis=0)

plt.imshow(out)
plt.show()

# CONV1
# Display filters on CONV1 layer
# the parameters are a list of [weights, biases]
filters = net.params['conv1'][0].data
vis_square(filters.transpose(0, 2, 3, 1))

# Display the response after CONV1 Layer
Out_layer1 = net.blobs['conv1'].data[0]
#vis_square(Out_layer1)

# CONV 2
# Display filters on Conv2 layer
# the parameters are a list of [weights, biases]
filters = net.params['conv2'][0].data
#print len(filters[0])
#vis_square(filters.transpose(0, 2, 3, 1))

# Display the response after CONV2 Layer
Out_layer2 = net.blobs['conv2'].data[0]
#vis_square(Out_layer2)


# CONV 3
# Display the response after CONV3 Layer
Out_layer3 = net.blobs['conv3'].data[0]
#vis_square(Out_layer3)

# CONV 4
print "conv4"
# Display the response after CONV4 Layer
Out_layer4 = net.blobs['conv4'].data[0]
#vis_square(Out_layer4)

# CONV 5
print "conv5"
# Display the response after CONV5 Layer
Out_layer5 = net.blobs['conv5'].data[0]
#vis_square(Out_layer5)

# FC6
print "FC6"
# Display the response after FC6 Layer
Out_layer6 = net.blobs['fc6'].data[0]
#vis_square(Out_layer6)


# FC7
print "FC7"
# Display the response after FC7 Layer
Out_layer7 = net.blobs['fc7'].data[0]
vis_square(Out_layer7)

# SCORE FC7
print "SCORE FC7"
# Display the response after FC7 Layer
Out_layerscr7 = net.blobs['score-fc7'].data[0]
vis_square(Out_layerscr7)

