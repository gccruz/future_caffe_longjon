#!/usr/bin/env python
# Goncalo Cruz

from __future__ import print_function
import sys
import re
import numpy as np
import matplotlib.pyplot as plt

from utils import strstr

def main():
  log_files = process_arguments(sys.argv)

  #Load file
  # f = open(log_files, 'r')
  # data=f.read()
  # f.close()

  # filter words
  # print(data.shape)

  # for line_indicator in range(1,len(data))
  train_iteration=[]
  train_loss=[]

  for index, line in enumerate(open(log_files, 'r' )):
    train_iteration.append(line.split()[0])
    train_loss.append(line.split()[1])

  time_list= []
  loss_list=[]

  print(train_loss)
  for indicator in range(len(train_loss)):
    try:
      loss_list.append(float(train_loss[indicator]))
    except:
      pass


  for indicator in range(len(train_iteration)):
    try:
      time_list.append(float(train_iteration[indicator]))
    except:
      pass

  print(time_list)
  print (loss_list)

  print(len(time_list))
  print(len(loss_list))

  # loss
  plt.plot(time_list, loss_list, 'r', label='Train loss')
#  plt.plot(test_iteration, test_loss, 'r', label='Test loss')
  plt.legend()
  plt.ylabel('Loss')
  plt.xlabel('Number of iterations')
  plt.savefig('loss.png')
  plt.show()



def process_arguments(argv):
  print(argv)
  if len(argv) < 1:
    help()

  log_files = argv[1]
  return log_files

def help():
  print('Usage: python loss_from_log.py [LOG_FILE]+\n'
        'LOG_FILE is text file containing log produced by caffe.'
        'At least one LOG_FILE has to be specified.'
        'Files has to be given in correct order (the oldest logs as the first ones).'
        , file=sys.stderr)

  exit()

if __name__ == '__main__':
  main()
